# Nama/Nim: Bram Musuko Panjaitan / 13517089


inputs = input().split(' ')
n = int(inputs[0])
k = int(inputs[1])

ans = ['*' if (i%k)==0 else str(i) for i in range(1,n+1)]
print(' '.join(ans))